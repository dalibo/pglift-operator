import pathlib
from typing import Any, Callable

import kopf
import kubernetes.client.models as k
import pytest
import yaml

from pglift_operator import models


def datapath(fname: str) -> pathlib.Path:
    here = pathlib.Path(__file__).parent
    return here / "data" / fname


ModelChecker = Callable[[str, Any], None]


class Dumper(yaml.SafeDumper):
    ...


def multiline_str(dumper: Any, data: str) -> yaml.Node:
    if "\n" in data:
        return dumper.represent_scalar("tag:yaml.org,2002:str", data, style="|")  # type: ignore[no-any-return]
    return dumper.represent_str(data)  # type: ignore[no-any-return]


Dumper.add_representer(str, multiline_str)


@pytest.fixture
def model_checker(regen_test_data: bool) -> ModelChecker:
    def clean_nones(data: dict[str, Any]) -> None:
        """Remove None values from 'data'."""
        for key, val in list(data.items()):
            if val is None:
                del data[key]
            elif isinstance(val, dict):
                clean_nones(val)
            elif isinstance(val, list):
                for item in val:
                    if isinstance(item, dict):
                        clean_nones(item)

    def checker(name: str, model: dict[str, Any]) -> None:
        clean_nones(model)
        path = datapath(f"{name}.yaml")
        if regen_test_data:
            with path.open("w") as f:
                yaml.dump(
                    model, f, Dumper=Dumper, allow_unicode=True, explicit_start=True
                )
            return
        with path.open() as f:
            expected = yaml.safe_load(f)
        assert model == expected

    return checker


@pytest.fixture
def name() -> str:
    return "test"


@pytest.fixture
def namespace() -> str:
    return "default"


@pytest.fixture
def body(name: str, namespace: str) -> kopf.Body:
    return kopf.Body(
        {
            "metadata": {"name": name, "namespace": namespace},
            "spec": {
                "replicas": 1,
                "ssl": True,
                "configuration": {"log_connections": True},
            },
        }
    )


@pytest.fixture
def configmap(name: str, namespace: str, body: kopf.Body) -> k.V1ConfigMap:
    return models.configmap(body.spec, name, namespace)


@pytest.fixture
def postgres_pvc() -> k.V1PersistentVolumeClaim:
    return models.postgres_pvc()


@pytest.fixture
def backup_pvc() -> k.V1PersistentVolumeClaim:
    return models.backup_pvc()


def test_configmap(model_checker: ModelChecker, configmap: k.V1ConfigMap) -> None:
    cm = configmap.to_dict()
    model_checker("cm", cm)


def test_leaderelection_configmap(model_checker: ModelChecker, name: str) -> None:
    cm = models.leaderelection_configmap(name).to_dict()
    model_checker("leaderelection_cm", cm)


def test_lsn_configmap(model_checker: ModelChecker, name: str) -> None:
    cm = models.lsn_configmap(name).to_dict()
    model_checker("lsn_cm", cm)


def test_postgres_pvc(
    model_checker: ModelChecker, postgres_pvc: k.V1PersistentVolumeClaim
) -> None:
    pvc = postgres_pvc.to_dict()
    model_checker("postgres-pvc", pvc)


def test_backup_pvc(
    model_checker: ModelChecker, backup_pvc: k.V1PersistentVolumeClaim
) -> None:
    pvc = backup_pvc.to_dict()
    model_checker("backup-pvc", pvc)


def test_statefulset(
    model_checker: ModelChecker,
    name: str,
    namespace: str,
    body: kopf.Body,
    configmap: k.V1ConfigMap,
    postgres_pvc: k.V1PersistentVolumeClaim,
    backup_pvc: k.V1PersistentVolumeClaim,
) -> None:
    sts = models.statefulset(
        configmap, postgres_pvc, backup_pvc, body.spec, name, namespace
    ).to_dict()
    model_checker("sts", sts)


def test_service(
    model_checker: ModelChecker, name: str, namespace: str, body: kopf.Body
) -> None:
    svc = models.service(name, namespace).to_dict()
    model_checker("svc", svc)


def test_service_primary(
    model_checker: ModelChecker, name: str, namespace: str, body: kopf.Body
) -> None:
    svc = models.service_primary(name, namespace).to_dict()
    model_checker("svc-primary", svc)


def test_cronjob(model_checker: ModelChecker, name: str, body: kopf.Body) -> None:
    cronjob = models.cronjob(name).to_dict()
    model_checker("cronjob", cronjob)


def test_service_account(
    model_checker: ModelChecker, name: str, body: kopf.Body
) -> None:
    sa = models.service_account(name).to_dict()
    model_checker("sa", sa)


def test_role(model_checker: ModelChecker, name: str, body: kopf.Body) -> None:
    role = models.role(name).to_dict()
    model_checker("role", role)


def test_role_binding(model_checker: ModelChecker, name: str, body: kopf.Body) -> None:
    rolebinding = models.role_binding(name).to_dict()
    model_checker("rolebinding", rolebinding)
