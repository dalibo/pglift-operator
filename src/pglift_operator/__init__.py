import time
from typing import Any, Optional, TypeVar

import kopf
import kubernetes.client.models as k
from kubernetes.client import AppsV1Api, BatchV1Api, CoreV1Api, RbacAuthorizationV1Api

from . import config, models

_Objs = TypeVar("_Objs")


def adopt(objs: _Objs, *args: Any, **kwargs: Any) -> _Objs:
    """Wraps kopf.adopt() to return adopted object(s)."""
    kopf.adopt(objs, *args, **kwargs)
    return objs


def log_created(kind: str, name: str, namespace: str, *, logger: kopf.Logger) -> None:
    logger.info(
        "created %(kind)s for instance %(name)s (namespace=%(namespace)s)",
        {"kind": kind, "name": name, "namespace": namespace},
    )


def log_replaced(kind: str, name: str, namespace: str, *, logger: kopf.Logger) -> None:
    logger.info(
        "replaced %(kind)s for instance %(name)s (namespace=%(namespace)s)",
        {"kind": kind, "name": name, "namespace": namespace},
    )


def _create_configmap(
    api: CoreV1Api,
    *,
    spec: kopf.Spec,
    name: str,
    namespace: str,
    logger: kopf.Logger,
) -> k.V1ConfigMap:
    cm = adopt(models.configmap(spec, name, namespace))
    r = api.create_namespaced_config_map(namespace, cm)
    log_created(r.kind, name, namespace, logger=logger)
    return cm


def _create_configmap_leaderelection(
    api: CoreV1Api, *, name: str, namespace: str, logger: kopf.Logger
) -> k.V1ConfigMap:
    cm = adopt(models.leaderelection_configmap(name))
    r = api.create_namespaced_config_map(namespace, cm)
    log_created(r.kind, cm.metadata.name, namespace, logger=logger)
    return cm


def _create_configmap_lsn(
    api: CoreV1Api, *, name: str, namespace: str, logger: kopf.Logger
) -> k.V1ConfigMap:
    cm = adopt(models.lsn_configmap(name))
    r = api.create_namespaced_config_map(namespace, cm)
    log_created(r.kind, cm.metadata.name, namespace, logger=logger)
    return cm


def _update_configmap(
    api: CoreV1Api,
    *,
    spec: kopf.Spec,
    name: str,
    namespace: str,
    logger: kopf.Logger,
) -> k.V1ConfigMap:
    cm = adopt(models.configmap(spec, name, namespace))
    cm_name = cm.metadata.name
    r = api.replace_namespaced_config_map(cm_name, namespace, cm)
    log_replaced(r.kind, name, namespace, logger=logger)
    return cm


def _create_statefulset(
    api: AppsV1Api,
    cm: k.V1ConfigMap,
    *,
    spec: kopf.Spec,
    name: str,
    namespace: str,
    logger: kopf.Logger,
) -> k.V1StatefulSet:
    postgres_pvc = adopt(models.postgres_pvc())
    backup_pvc = adopt(models.backup_pvc())
    sts = adopt(models.statefulset(cm, postgres_pvc, backup_pvc, spec, name, namespace))
    r = api.create_namespaced_stateful_set(namespace, sts)
    log_created(r.kind, name, namespace, logger=logger)
    return sts


def _update_statefulset(
    api: AppsV1Api,
    cm: k.V1ConfigMap,
    *,
    spec: kopf.Spec,
    name: str,
    namespace: str,
    logger: kopf.Logger,
) -> k.V1StatefulSet:
    postgres_pvc = adopt(models.postgres_pvc())
    backup_pvc = adopt(models.backup_pvc())
    sts = adopt(models.statefulset(cm, postgres_pvc, backup_pvc, spec, name, namespace))
    sts_name = sts.metadata.name
    r = api.replace_namespaced_stateful_set(sts_name, namespace, sts)
    log_replaced(r.kind, name, namespace, logger=logger)
    return sts


def _create_services(
    api: CoreV1Api, *, name: str, namespace: str, logger: kopf.Logger
) -> None:
    for handler in (models.service, models.service_primary):
        svc = adopt(handler(name, namespace))
        r = api.create_namespaced_service(namespace, svc)
        log_created(r.kind, name, namespace, logger=logger)


def _create_service_account(
    api: CoreV1Api, *, name: str, namespace: str, logger: kopf.Logger
) -> None:
    sa = adopt(models.service_account(name))
    r = api.create_namespaced_service_account(namespace, sa)
    log_created(r.kind, name, namespace, logger=logger)


def _create_rbac(
    api: RbacAuthorizationV1Api, *, name: str, namespace: str, logger: kopf.Logger
) -> None:
    for model, create in (
        (models.role, api.create_namespaced_role),
        (models.role_binding, api.create_namespaced_role_binding),
    ):
        obj = adopt(model(name))
        r = create(namespace, obj)
        log_created(r.kind, name, namespace, logger=logger)


def _create_cron_job(
    api: BatchV1Api, *, name: str, namespace: str, logger: kopf.Logger
) -> None:
    cronjob = adopt(models.cronjob(name))
    r = api.create_namespaced_cron_job(namespace, cronjob)
    log_created(r.kind, name, namespace, logger=logger)


def get_primary(api: CoreV1Api, *, name: str, namespace: str) -> str:
    svc = api.read_namespaced_service(
        name=models.service_primary_name(name), namespace=namespace
    )
    primary = svc.spec.selector["statefulset.kubernetes.io/pod-name"]
    assert isinstance(primary, str), primary
    return primary


@kopf.on.create(kind="Instance")
def create_instance(
    *,
    spec: kopf.Spec,
    name: Optional[str],
    namespace: Optional[str],
    logger: kopf.Logger,
    **_: Any,
) -> None:
    # TODO: handle those None
    assert name is not None
    assert namespace is not None
    core = CoreV1Api()
    apps = AppsV1Api()
    batch = BatchV1Api()
    rbac = RbacAuthorizationV1Api()
    cm = _create_configmap(
        core, spec=spec, name=name, namespace=namespace, logger=logger
    )
    _create_services(core, name=name, namespace=namespace, logger=logger)
    _create_service_account(core, name=name, namespace=namespace, logger=logger)
    _create_rbac(rbac, name=name, namespace=namespace, logger=logger)
    _create_configmap_leaderelection(
        core, name=name, namespace=namespace, logger=logger
    )
    _create_configmap_lsn(core, name=name, namespace=namespace, logger=logger)
    _create_statefulset(
        apps, cm, spec=spec, name=name, namespace=namespace, logger=logger
    )
    _create_cron_job(batch, name=name, namespace=namespace, logger=logger)


@kopf.on.update(kind="Instance")
def update_instance(
    *,
    spec: kopf.Spec,
    name: Optional[str],
    namespace: Optional[str],
    logger: kopf.Logger,
    **_: Any,
) -> None:
    # TODO: handle those None
    assert name is not None
    assert namespace is not None
    api = CoreV1Api()
    apps = AppsV1Api()
    cm = _update_configmap(
        api, spec=spec, name=name, namespace=namespace, logger=logger
    )
    _update_statefulset(
        apps, cm, spec=spec, name=name, namespace=namespace, logger=logger
    )


@kopf.on.create(kind="InstanceBackup")
def handle_instancebackup(
    *,
    body: kopf.Body,
    name: Optional[str],
    namespace: Optional[str],
    logger: kopf.Logger,
    **_: Any,
) -> None:
    # TODO: handle those None
    assert name is not None
    assert namespace is not None
    backup_type = body["spec"]["type"]
    job = {
        "metadata": {
            "name": f"{name}-{backup_type}-backup-{int(time.time())}",
            "labels": {"backup": backup_type},
        },
        "spec": {
            "template": {
                "spec": {
                    "restartPolicy": "Never",
                    "backoffLimit": 4,
                    "volumes": [
                        {
                            "name": config.PGHOME_VOLUME,
                            "persistentVolumeClaim": {
                                "claimName": f"{config.PGHOME_VOLUME}-{name}-0"
                            },
                        },
                        {
                            "name": config.BACKUP_VOLUME,
                            "persistentVolumeClaim": {
                                "claimName": f"{config.BACKUP_VOLUME}-{name}-0"
                            },
                        },
                        {
                            "name": "config",
                            "configMap": {
                                "name": name,
                            },
                        },
                    ],
                    "imagePullSecrets": config.image_pull_secrets(),
                    "containers": [
                        {
                            "name": "pglift-backup",
                            "image": config.PGLIFT_IMAGE,
                            "imagePullPolicy": config.IMAGE_PULL_POLICY,
                            "volumeMounts": [
                                models.backup_volumemount(),
                                models.postgres_home_volumemount(read_only=True),
                                models.postgres_settings_volumemount(),
                            ],
                            "command": [
                                "pglift",
                                "-Ldebug",
                                "instance",
                                "backup",
                                name,
                                f"--type={backup_type}",
                            ],
                        },
                    ],
                },
            },
        },
    }
    # TODO: Also reference the Instance.
    kopf.adopt(job)
    r = BatchV1Api().create_namespaced_job(namespace, job)
    log_created(r.kind, name, namespace, logger=logger)
