FROM debian:bullseye-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -qq update && \
	apt-get -qq -y install --no-install-recommends \
	curl \
	ca-certificates \
	gnupg \
	lsb-release \
	&& rm -rf /var/lib/apt/lists/*
ARG UID=999
ARG GID=999
RUN set -eux; \
	groupadd -r postgres --gid=${GID}; \
	useradd -r -g postgres --uid=${UID} --home-dir=/var/lib/postgresql --shell=/bin/bash postgres; \
	mkdir -p /var/lib/postgresql; \
	chown -R postgres:postgres /var/lib/postgresql

RUN echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -c -s)-pgdg main" \
	| tee /etc/apt/sources.list.d/pgdg.list
RUN curl --output /etc/apt/trusted.gpg.d/pgdg.asc https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN apt-get -qq update && \
	apt-get -y install --no-install-recommends postgresql-common
RUN mkdir -p /etc/postgresql-common/createcluster.d/
RUN echo "create_main_cluster = false" \
	> /etc/postgresql-common/createcluster.d/no-create-main-cluster.conf
RUN apt-get -qq update && \
	apt-get -y install --no-install-recommends \
	postgresql-14 \
	pgbackrest \
	&& rm -rf /var/lib/apt/lists/*

USER postgres
WORKDIR /var/lib/postgresql

#  https://github.com/docker-library/postgres/blob/a1ea032a8b5872e291f5f3f7b8395b8e958aaefb/Dockerfile-debian.template#L203
STOPSIGNAL SIGINT
