import functools
import socket
from typing import Optional

import click
import click.exceptions
import kubernetes.config

from . import get_primary


@click.group()
def cli() -> None:
    pass


@functools.cache
def load_kube_config() -> None:
    try:
        kubernetes.config.load_incluster_config()
    except kubernetes.config.config_exception.ConfigException:
        kubernetes.config.load_kube_config()


def current_namespace() -> str:
    load_kube_config()
    try:
        with open("/var/run/secrets/kubernetes.io/serviceaccount/namespace") as f:
            return f.read()
    except FileNotFoundError:
        namespace = kubernetes.config.list_kube_config_contexts()[1]["context"][
            "namespace"
        ]
        assert isinstance(namespace, str), namespace
        return namespace


@cli.command("iamprimary")
@click.argument("instance")
@click.option("--namespace", required=False)
@click.option("--identity", required=False)
def iamprimary(
    instance: str, namespace: Optional[str], identity: Optional[str]
) -> None:
    if not namespace:
        namespace = current_namespace()
    if identity is None:
        identity = socket.gethostname()
    api = kubernetes.client.CoreV1Api()
    primary = get_primary(api, name=instance, namespace=namespace)
    if primary != identity:
        click.echo(f"primary is {primary}", err=True)
        raise click.exceptions.Exit(1)
