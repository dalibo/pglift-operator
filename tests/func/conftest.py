import shutil

import pytest


@pytest.fixture(scope="session")
def kubectl_cmd() -> str:
    prog = shutil.which("kubectl")
    if prog is None:
        pytest.skip("kubectl command not found")
    return prog
