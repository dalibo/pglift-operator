# Work in progress pglift k8s operator

## About

This project provides a Kubernetes operator based on [pglift][]. It handles a
custom resource definition defined in [k8s/instance.yaml][] and a set of
Docker image definitions for running a pglift "instance", as a StatefulSet of
two containers (one for the PostgreSQL instance and one for Prometheus
postgres\_exporter) along with a CronJob for scheduled backups.

## Setup

The first thing to do is to install the CRDs and other Kubernetes objects:
```
$ kubectl apply -f k8s
```

The following setup instructions suggest to use minikube for local
development or to use a container registry otherwise.

### Minikube

Start minikube (`minikube start`), build and push Docker images into
[in-cluster Docker daemon][]:

```
$ eval $(minikube docker-env)
$ make -j build
```

Then make sure all images are listed:
```
$ minikube image ls | grep pglift
docker.io/pglift/operator:latest
docker.io/pglift/postgresql:latest
docker.io/pglift/postgres_exporter:latest
```

[in-cluster Docker daemon]: https://minikube.sigs.k8s.io/docs/handbook/pushing/#1-pushing-directly-to-the-in-cluster-docker-daemon-docker-env

### Using a container registry

First log into the target registry, e.g.:
```
$ docker login $REGISTRY -u <login> -p ...
```
Then build and push the images:
```
$ make -j push REGISTRY=$REGISTRY  # $REGISTRY should end with a /
```

## Running the operator

### Locally

First create a virtualenv and install run-time dependencies:
```
$ python3 -m venv .venv
$ . .venv/bin/activate
(.venv) $ pip install -e .
```

Then run the operator as:
```
(.venv) $ kopf run -m pglift_operator
```

### In-cluster

#### With imagePullPolicy to Never/IfNotPresent and default registry

```
$ IMAGE_PULL_POLICY=IfNotPresent
$ kubectl apply -k install/base/
```

This assumes that the pglift/operator image is available (from build/push
steps in setup section above).

#### With imagePullPolicy to Always and custom registry

This section explains how to use a remote registry to download the
pglift/operator image and all the other images to run your pglift
instances. The commands below also set the imagePullPolicy to Always,
feel free to adapt them if you need a more conservative strategy.

If you use a private registry, you will probably need to define a secret to
pull your images. Currently this secret must be named `pglift-registry-secret`,
you can create it with the following command:
```console
kubectl create secret docker-registry pglift-registry-secret \
    --docker-server=xxxx --docker-username=xxxx --docker-password=xxxx \
    --docker-email=xxx
```

Then you can deploy the operator with those commands:
```console
$ IMAGE_PULL_POLICY=Always
$ REGISTRY=http://my.private.registry.lan
$ cd install/custom-registry-policy/
$ kustomize edit set image pglift/operator="${REGISTRY}pglift/operator:latest"
$ kustomize build ./ | kubectl apply -f -
```

## Usage

The following manifest describes an Instance resource that would be managed by
the operator:
```yaml
apiVersion: pglift.dalibo.com/v1alpha1
kind: Instance
metadata:
  name: main
spec:
  ssl: true
  configuration:
    work_mem: 32MB
```
Save it to `instance.yaml` and run:
```
$ kubectl apply -f instance.yaml
instance.pglift.dalibo.com/main created
```

If the operator is in-cluster, some resources should start getting created;
otherwise, run the operator once (locally, e.g. in another console) as:
```
(.venv) $ kopf run -m pglift_operator
```

After a moment, resources should be created:
```
$ kubectl get instances,statefulsets,pods,svc,cronjobs
NAME                              AGE
instance.pglift.dalibo.com/main   39s

NAME                    READY   AGE
statefulset.apps/main   1/1     33s

NAME         READY   STATUS    RESTARTS   AGE
pod/main-0   3/3     Running   0          33s

NAME                          TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)             AGE
service/kubernetes            ClusterIP   10.32.0.1    <none>        443/TCP             31m
service/pglift-main           ClusterIP   None         <none>        5432/TCP,9187/TCP   20s
service/pglift-main-primary   ClusterIP   10.38.0.34   <none>        5432/TCP,9187/TCP   20s

NAME                        SCHEDULE     SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/main-backup   12 * * * *   False     0        <none>          33s
```

The `Instance` resource, as defined in [k8s/instance.yaml][], maps to:

* a `StatefulSet` with a pod running tree containers (the `postgres` one for the
  PostgreSQL instance, the `postgres-exporter` one for Prometheus
  postgres\_exporter and a `leaderelection` container managing
  high-availability when multiple replicas are present), along with,
* a `CronJob` for scheduled backup with pgbackrest.

The operator supports modification of `Instance` resources. E.g.:

```
$ kubectl patch instance main --type merge --patch '{"spec":{"configuration":{"work_mem":"4MB"}}}'
instance.pglift.dalibo.com/main patched
```
```
$ kubectl get instance main -o json | jq .spec.configuration
{
  "work_mem": "4MB"
}
$ kubectl exec -t main-0 -c postgres -- \
    psql -t -c "select setting from pg_settings where name = 'work_mem'"
 4096
```

Scaling the instance set up or down adds or removes standby instances:
```
$ kubectl scale --replicas=2 instance main
$ kubectl rollout status statefulset main
Waiting for 1 pods to be ready...
partitioned roll out complete: 3 new pods have been updated...
$ kubectl get pods
NAME     READY   STATUS    RESTARTS   AGE
main-0   3/3     Running   0          43s
main-1   3/3     Running   0          11s
```

Getting a Postgres client (e.g. on the primary `main-0` if there's only one
replicas or the leader otherwise):
```
$ kubectl run pgclient --rm -it \
    --image=pglift/postgresql --image-pull-policy=Never --restart=Never \
    -- psql "host=pglift-main-primary"
postgres=#
```

## Running tests

First install tests dependencies as:
```
(.venv) $ pip install -e ".[test]"
```
Then run unit tests:
```
(.venv) $ pytest tests/unit
```

Functional tests a real Kubernetes cluster, possibly minikube, with CRDs
installed (the operator does not need to be installed). Then run:
```
(.venv) $ pytest tests/func
```
These tests use a dedicated namespace created and destroyed for each session;
the value of this namespace can be set through `PGLIFT_OP_NS` environment
variable.

## Limitations / TODO

* pgbackrest configuration, esp. archive_command, is wrong when the pgbackrest
  volume is not available; also, per current archive_command, the pgbackrest
  executable needs to be present in the postgres container

* creating the Instance with a `replicas` value greater than one fails during
  initialization of the first stanbdy, apparently because the headless Service
  is not yet active

* scaling the statefulset will be reverted by the operator; we can only scale
  instances

* the operator does not pull images, this is a short-cut to help local
  development

* record `Events` on the `Instance` resource (to be displayed with `kubectl
  describe instance main` for instance)

* set `Version` field on `Instance` after Statefulset creation

* `postgres_exporter` container contains pglift; should be changed similarly
  to `postgres` container: generate a configmap entry (from-env-file, then
  used in pod through `envFrom.configMapRef`) for `postgres_exporter` from the
  operator and then simply use a plain `postgres-exporter` container

[pglift]: https://gitlab.com/dalibo/pglift
[k8s/instance.yaml]: ./k8s/instance.yaml
