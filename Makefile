REGISTRY=

IMAGES=operator postgres_exporter postgresql
IMAGE_BUILD_TARGETS=$(addprefix build-, $(IMAGES))
IMAGE_PUSH_TARGETS=$(addprefix push-, $(IMAGES))

build: $(IMAGE_BUILD_TARGETS)

push: $(IMAGE_PUSH_TARGETS)

build-operator:
	docker build . -t ${REGISTRY}pglift/operator

build-%: docker/%
	docker build $< -t ${REGISTRY}pglift/$*

push-%: build-%
	docker push ${REGISTRY}pglift/$*
