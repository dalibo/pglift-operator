import hashlib
import json
from pathlib import Path
from typing import Optional

import kopf
import kubernetes.client.models as k
import yaml
from pglift.models import system
from pglift.settings import Settings

from . import config


def yaml_dump_settings(s: Settings) -> str:
    return yaml.safe_dump(json.loads(s.json()))  # type: ignore[no-any-return]


def app_label(name: str) -> str:
    return f"pglift-{name}"


def service_name(name: str) -> str:
    return f"pglift-{name}"


def service_primary_name(name: str) -> str:
    return f"pglift-{name}-primary"


def leaderelection_configmap_name(name: str) -> str:
    return f"{name}-leaderelection"


def lsn_configmap_name(name: str) -> str:
    return f"{name}-lsn"


def service_account_name(name: str) -> str:
    return f"pglift-{name}"


def config_map_volume(name: str) -> k.V1Volume:
    return k.V1Volume(
        name="config",
        config_map=k.V1ConfigMapVolumeSource(name=name),
    )


def postgres_home_volumemount(*, read_only: Optional[bool] = None) -> k.V1VolumeMount:
    return k.V1VolumeMount(
        name=config.PGHOME_VOLUME, mount_path=config.PGHOME, read_only=read_only
    )


def postgres_settings_volumemount() -> k.V1VolumeMount:
    return k.V1VolumeMount(
        name="config",
        mount_path="/etc/pglift/settings.yaml",
        sub_path="settings_postgresql.yaml",
    )


def backup_volumemount() -> k.V1VolumeMount:
    return k.V1VolumeMount(name=config.BACKUP_VOLUME, mount_path=config.BACKUP_DIR)


def postgres_exporter_home_volumemount() -> k.V1VolumeMount:
    return k.V1VolumeMount(
        name=config.PROMETHEUS_VOLUME, mount_path=config.PROMETHEUS_HOME
    )


def postgres_exporter_settings_volumemount() -> k.V1VolumeMount:
    return k.V1VolumeMount(
        name="config",
        mount_path="/etc/pglift/settings.yaml",
        sub_path="settings_postgres_exporter.yaml",
    )


def postgres_exporter_manifest_volumemount(
    mount_path: str,
) -> k.V1VolumeMount:
    return k.V1VolumeMount(
        name="config",
        mount_path=mount_path,
        sub_path="postgres_exporter.yaml",
    )


def instance_apply_container(
    instance: system.PostgreSQLInstance,
) -> k.V1Container:
    primary_manifest_path = str(Path(config.PGHOME) / "primary.yaml")
    standby_manifest_path = str(Path(config.PGHOME) / "standby.yaml")
    command = [
        "bash",
        "-c",
        "\n".join(
            [
                "set -ex",
                f"if pglift-operator iamprimary {instance.name}; then",
                f"  manifest={primary_manifest_path}",
                "else",
                f"  manifest={standby_manifest_path}",
                "fi",
                "pglift -Ldebug instance apply -f $manifest",
            ]
        ),
    ]
    return k.V1Container(
        name="pglift-instance-apply",
        image=config.PGLIFT_IMAGE,
        image_pull_policy=config.IMAGE_PULL_POLICY,
        volume_mounts=[
            postgres_home_volumemount(),
            backup_volumemount(),
            postgres_settings_volumemount(),
            k.V1VolumeMount(
                name="config",
                mount_path="/etc/pglift/postgresql/site.conf",
                sub_path="k8s-postgresql.conf",
            ),
            k.V1VolumeMount(
                name="config",
                mount_path="/etc/pglift/postgresql/pg_hba.conf",
                sub_path="k8s-pg_hba.conf",
            ),
            k.V1VolumeMount(
                name="config",
                mount_path=primary_manifest_path,
                sub_path="primary.yaml",
            ),
            k.V1VolumeMount(
                name="config",
                mount_path=standby_manifest_path,
                sub_path="standby.yaml",
            ),
        ],
        command=command,
    )


def postgres_container(
    instance: system.PostgreSQLInstance,
) -> k.V1Container:
    assert config.SETTINGS.postgresql.bindir is not None
    bindir = Path(config.SETTINGS.postgresql.bindir.format(version=instance.version))
    return k.V1Container(
        name="postgres",
        image=config.POSTGRES_IMAGE,
        image_pull_policy=config.IMAGE_PULL_POLICY,
        volume_mounts=[
            postgres_home_volumemount(),
        ],
        env=[
            k.V1EnvVar(
                name="PGDATA",
                value=str(instance.datadir),
            ),
            k.V1EnvVar(
                # Use for local psql usage.
                name="PGHOST",
                value=str(config.SETTINGS.postgresql.socket_directory),
            ),
        ],
        command=[str(bindir / "postgres")],
        readiness_probe=k.V1Probe(
            _exec=k.V1ExecAction(command=[str(bindir / "pg_isready")]),
            initial_delay_seconds=2,
            period_seconds=2,
        ),
    )


def postgres_exporter_apply_container() -> k.V1Container:
    manifest_path = str(Path(config.PGHOME) / "exporter.yaml")
    return k.V1Container(
        name="pglift-postgres-exporter-apply",
        image=config.POSTGRES_EXPORTER_IMAGE,
        image_pull_policy=config.IMAGE_PULL_POLICY,
        command=[
            "pglift",
            "-Ldebug",
            "postgres_exporter",
            "apply",
            "-f",
            manifest_path,
        ],
        volume_mounts=[
            postgres_exporter_home_volumemount(),
            postgres_exporter_settings_volumemount(),
            postgres_exporter_manifest_volumemount(manifest_path),
        ],
    )


def postgres_exporter_container(name: str) -> k.V1Container:
    return k.V1Container(
        name="postgres-exporter",
        image=config.POSTGRES_EXPORTER_IMAGE,
        image_pull_policy=config.IMAGE_PULL_POLICY,
        volume_mounts=[
            postgres_exporter_home_volumemount(),
            postgres_exporter_settings_volumemount(),
        ],
        command=[
            "pglift",
            "-Ldebug",
            "postgres_exporter",
            "start",
            "--foreground",
            name,
        ],
        readiness_probe=k.V1Probe(
            http_get=k.V1HTTPGetAction(port=9187),
            initial_delay_seconds=2,
            period_seconds=2,
        ),
    )


def pgbackrest_container(command: list[str]) -> k.V1Container:
    return k.V1Container(
        name="pgbackrest",
        image=config.PGLIFT_IMAGE,
        image_pull_policy=config.IMAGE_PULL_POLICY,
        volume_mounts=[
            backup_volumemount(),
            postgres_home_volumemount(read_only=True),
            postgres_settings_volumemount(),
        ],
        command=command,
    )


def leaderelection_container(name: str, namespace: str) -> k.V1Container:
    return k.V1Container(
        name="leaderelection",
        image=config.PGLIFT_IMAGE,
        image_pull_policy=config.IMAGE_PULL_POLICY,
        command=[
            "python3",
            "-m",
            "pglift_operator.leaderelection",
            name,
            "--namespace",
            namespace,
        ],
    )


def leaderelection_configmap(name: str) -> k.V1ConfigMap:
    return k.V1ConfigMap(
        metadata=k.V1ObjectMeta(name=leaderelection_configmap_name(name))
    )


def configmap(spec: kopf.Spec, name: str, namespace: str) -> k.V1ConfigMap:
    ssl = spec.get("ssl", False)
    configuration = spec.get("configuration", {})
    instance_manifest = {
        "name": name,
        "configuration": configuration,
        "ssl": ssl,
        "state": "stopped",
    }
    primary_manifest = yaml.dump(instance_manifest)
    standby_manifest = yaml.dump(
        dict(
            instance_manifest,
            standby={"for": f"host={service_primary_name(name)} user=replication"},
        )
    )
    postgres_exporter_manifest = yaml.dump(
        {
            "name": name,
            "port": 9187,
            "dsn": "host=localhost user=postgres",
            "state": "stopped",
        }
    )
    # Only set unix_socket_directories in postgresql/site.conf so that we can later use this value
    # to configure pgbackrest. Other "default" settings are meaningless in k8s context.
    k8s_postgresql_conf = (
        "\n".join(
            [
                "unix_socket_directories = {settings.socket_directory}",
                "listen_addresses = '*'",
            ]
        )
        + "\n"
    )
    # TODO: restrict replication to DNS domain name of the StatefuleSet (i.e.
    # service_name(instance)).
    k8s_pg_hba_conf = "\n".join(
        [
            "local   all             postgres                    peer",
            "local   all             backup                      trust",
            "host    all             all             all         trust",
            "host    all             all             ::1/128     trust",
            "host    replication     replication     all         trust",
            "host    replication     replication     ::1/128     trust",
        ]
    )
    return k.V1ConfigMap(
        metadata=k.V1ObjectMeta(name=name),
        data={
            "primary.yaml": primary_manifest,
            "standby.yaml": standby_manifest,
            "postgres_exporter.yaml": postgres_exporter_manifest,
            "settings_postgresql.yaml": yaml_dump_settings(config.SETTINGS),
            "settings_postgres_exporter.yaml": yaml_dump_settings(
                config.POSTGRES_EXPORTER_SETTINGS
            ),
            "k8s-postgresql.conf": k8s_postgresql_conf,
            "k8s-pg_hba.conf": k8s_pg_hba_conf,
        },
    )


def lsn_configmap(name: str) -> k.V1ConfigMap:
    return k.V1ConfigMap(metadata=k.V1ObjectMeta(name=lsn_configmap_name(name)))


def postgres_pvc() -> k.V1PersistentVolumeClaim:
    return k.V1PersistentVolumeClaim(
        metadata=k.V1ObjectMeta(name=config.PGHOME_VOLUME),
        spec=k.V1PersistentVolumeClaimSpec(
            access_modes=["ReadWriteOnce"],
            resources=k.V1ResourceRequirements(
                requests={"storage": config.PGHOME_SIZE}
            ),
        ),
    )


def backup_pvc() -> k.V1PersistentVolumeClaim:
    return k.V1PersistentVolumeClaim(
        metadata=k.V1ObjectMeta(name=config.BACKUP_VOLUME),
        spec=k.V1PersistentVolumeClaimSpec(
            access_modes=["ReadWriteOnce"],
            resources=k.V1ResourceRequirements(
                requests={"storage": config.BACKUP_SIZE}
            ),
        ),
    )


def statefulset(
    cm: k.V1ConfigMap,
    postgres_pvc: k.V1PersistentVolumeClaim,
    backup_pvc: k.V1PersistentVolumeClaim,
    spec: kopf.Spec,
    name: str,
    namespace: str,
) -> k.V1StatefulSet:
    manifest_md5sum = hashlib.md5(cm.data["primary.yaml"].encode()).hexdigest()
    pglift_instance = system.PostgreSQLInstance(
        name, config.PGVERSION, settings=config.SETTINGS
    )
    return k.V1StatefulSet(
        metadata=k.V1ObjectMeta(name=name),
        spec=k.V1StatefulSetSpec(
            replicas=spec["replicas"],
            selector=k.V1LabelSelector(match_labels={"app": app_label(name)}),
            service_name=service_name(name),
            template=k.V1PodTemplateSpec(
                metadata=k.V1ObjectMeta(
                    labels={"app": app_label(name)},
                    annotations={"pglift.dalibo.com/manifestMd5sum": manifest_md5sum},
                ),
                spec=k.V1PodSpec(
                    service_account_name=service_account_name(name),
                    termination_grace_period_seconds=2,
                    security_context=k.V1PodSecurityContext(
                        fs_group=config.PGGID,
                        fs_group_change_policy="OnRootMismatch",
                    ),
                    volumes=[
                        config_map_volume(name),
                        k.V1Volume(
                            name=config.PROMETHEUS_VOLUME,
                            empty_dir=k.V1EmptyDirVolumeSource(),
                        ),
                    ],
                    image_pull_secrets=config.image_pull_secrets(),
                    init_containers=[
                        instance_apply_container(pglift_instance),
                        postgres_exporter_apply_container(),
                    ],
                    containers=[
                        postgres_container(pglift_instance),
                        postgres_exporter_container(name),
                        leaderelection_container(name, namespace),
                    ],
                ),
            ),
            volume_claim_templates=[postgres_pvc, backup_pvc],
        ),
    )


def service(name: str, namespace: str) -> k.V1Service:
    """Return a headless service that governs the StatefulSet and is
    responsible for the network identity of the set.

    This service must exist before the StatefulSet.

    https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#limitations
    https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/stateful-set-v1/#StatefulSetSpec
    """
    return k.V1Service(
        metadata=k.V1ObjectMeta(
            name=service_name(name),
            labels={
                "app": app_label(name),
            },
        ),
        spec=k.V1ServiceSpec(
            selector={
                "app": app_label(name),
            },
            type="ClusterIP",
            cluster_ip="None",
            ports=[
                k.V1ServicePort(
                    name="postgres",
                    port=5432,
                ),
                k.V1ServicePort(
                    name="postgres-exporter",
                    port=9187,
                ),
            ],
        ),
    )


def service_primary(name: str, namespace: str) -> k.V1Service:
    return k.V1Service(
        metadata=k.V1ObjectMeta(
            name=service_primary_name(name),
            labels={
                "app": app_label(name),
            },
        ),
        spec=k.V1ServiceSpec(
            selector={
                "app": app_label(name),
                "statefulset.kubernetes.io/pod-name": f"{name}-0",
            },
            ports=[
                k.V1ServicePort(
                    name="postgres",
                    port=5432,
                ),
                k.V1ServicePort(
                    name="postgres-exporter",
                    port=9187,
                ),
            ],
        ),
    )


def cronjob(name: str) -> k.V1CronJob:
    return k.V1CronJob(
        metadata=k.V1ObjectMeta(
            name=f"{name}-backup",
        ),
        spec=k.V1CronJobSpec(
            schedule="0 * * * *",
            job_template=k.V1JobTemplateSpec(
                spec=k.V1JobSpec(
                    template=k.V1PodTemplateSpec(
                        spec=k.V1PodSpec(
                            restart_policy="Never",
                            volumes=[
                                k.V1Volume(
                                    name=config.BACKUP_VOLUME,
                                    persistent_volume_claim=k.V1PersistentVolumeClaimVolumeSource(
                                        claim_name=f"{config.BACKUP_VOLUME}-{name}-0"
                                    ),
                                ),
                                k.V1Volume(
                                    name=config.PGHOME_VOLUME,
                                    persistent_volume_claim=k.V1PersistentVolumeClaimVolumeSource(
                                        claim_name=f"{config.PGHOME_VOLUME}-{name}-0"
                                    ),
                                ),
                                config_map_volume(name),
                            ],
                            image_pull_secrets=config.image_pull_secrets(),
                            containers=[
                                pgbackrest_container(
                                    [
                                        "pglift",
                                        "-Ldebug",
                                        "instance",
                                        "backup",
                                        name,
                                    ]
                                ),
                            ],
                        ),
                    ),
                ),
            ),
        ),
    )


def service_account(name: str) -> k.V1ServiceAccount:
    return k.V1ServiceAccount(metadata=k.V1ObjectMeta(name=service_account_name(name)))


def role(name: str) -> k.V1Role:
    return k.V1Role(
        metadata=k.V1ObjectMeta(name=service_account_name(name)),
        rules=[
            k.V1PolicyRule(
                api_groups=[""],
                resources=["configmaps"],
                resource_names=[leaderelection_configmap_name(name)],
                verbs=["get", "update"],
            ),
            k.V1PolicyRule(
                api_groups=[""],
                resources=["configmaps"],
                resource_names=[lsn_configmap_name(name)],
                verbs=["get", "patch"],
            ),
            k.V1PolicyRule(
                api_groups=[""],
                resources=["services"],
                resource_names=[service_primary_name(name)],
                verbs=["get", "update"],
            ),
        ],
    )


def role_binding(name: str) -> k.V1RoleBinding:
    rbac_name = service_account_name(name)
    return k.V1RoleBinding(
        metadata=k.V1ObjectMeta(name=rbac_name),
        role_ref=k.V1RoleRef(
            api_group="rbac.authorization.k8s.io", kind="Role", name=rbac_name
        ),
        subjects=[k.V1Subject(kind="ServiceAccount", name=rbac_name)],
    )
