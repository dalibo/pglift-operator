import json
import os
import pathlib
import subprocess
import time
from contextlib import contextmanager
from typing import Callable, Iterator, Tuple, Union

import port_for
import psycopg
import pytest
import requests
import yaml
from kopf.testing import KopfRunner
from tenacity import retry
from tenacity.stop import stop_after_attempt
from tenacity.wait import wait_fixed

CommandRunner = Callable[..., subprocess.CompletedProcess[str]]


@contextmanager
def operator_running(namespace: str) -> Iterator[None]:
    with KopfRunner(["run", "--standalone", "-n", namespace, "-m", "pglift_operator"]):
        yield


@pytest.fixture(scope="module")
def namespace(kubectl_cmd: str) -> Iterator[str]:
    ns = os.environ.get("PGLIFT_OP_NS", f"pglift-tests-{int(time.time())}")
    subprocess.run(["kubectl", "create", "namespace", ns], check=True)
    yield ns
    subprocess.run(["kubectl", "delete", "namespace", ns], check=True)


@pytest.fixture(scope="module")
def kubectl(kubectl_cmd: str, namespace: str) -> CommandRunner:
    def fn(*args: Union[str, pathlib.Path]) -> subprocess.CompletedProcess[str]:
        cmd = [kubectl_cmd, "-n", namespace] + [str(a) for a in args]
        return subprocess.run(cmd, capture_output=True, check=True, text=True)

    return fn


@retry(stop=stop_after_attempt(5), wait=wait_fixed(2))
def retrying(runner: CommandRunner, *args: str) -> subprocess.CompletedProcess[str]:
    return runner(*args)


@pytest.fixture
def instance(
    namespace: str, kubectl: CommandRunner, tmp_path: pathlib.Path
) -> Iterator[str]:
    name = "test"
    data = {
        "apiVersion": "pglift.dalibo.com/v1alpha1",
        "kind": "Instance",
        "metadata": {
            "name": name,
        },
        "spec": {
            "ssl": True,
            "configuration": {
                "log_line_prefix": "",
                "log_replication_commands": True,
                "work_mem": "32MB",
            },
        },
    }
    fpath = tmp_path / "instance.yaml"
    with fpath.open("w") as f:
        yaml.safe_dump(data, f)
    kubectl("apply", "-f", fpath)

    yield name

    kubectl("delete", "--cascade=foreground", "--wait=true", "instance", name)
    r = kubectl("get", "-o", "json", "instances,statefulsets,cronjobs,pods,pvc,cm")
    items = [
        item
        for item in json.loads(r.stdout)["items"]
        if item["metadata"]["name"] != "kube-root-ca.crt"
    ]
    assert not items


@contextmanager
def port_forward(namespace: str, instance: str) -> Iterator[Tuple[int, int]]:
    """Run kubectl port-format for postgres and postgres-exporter ports in a
    background process and yield respective local ports.
    """
    pg_port = port_for.select_random()
    pge_port = port_for.select_random(exclude_ports=[pg_port])
    cmd = [
        "kubectl",
        "-n",
        namespace,
        "port-forward",
        f"svc/pglift-{instance}",
        f"{pg_port}:postgres",
        f"{pge_port}:postgres-exporter",
    ]
    proc = subprocess.Popen(cmd)
    time.sleep(1)
    try:
        yield pg_port, pge_port
    finally:
        proc.terminate()


def test_empty(kubectl: CommandRunner) -> None:
    r = kubectl(*"get -o json instances,statefulsets,cronjobs,pods".split())
    result = json.loads(r.stdout)
    assert result["items"] == []


def test_create_update(
    kubectl: CommandRunner, namespace: str, instance: str, tmp_path: pathlib.Path
) -> None:
    with operator_running(namespace):
        # Wait until the sts gets created.
        retrying(kubectl, "get", "statefulset", instance)
        kubectl("rollout", "status", "-w", "--timeout=60s", "statefulset", instance)

    sts = json.loads(kubectl(*"get -o json statefulsets".split()).stdout)
    assert len(sts["items"]) == 1
    assert sts["items"][0]["metadata"]["name"] == instance

    pods = json.loads(kubectl(*"get -o json pods".split()).stdout)
    (pod,) = pods["items"]  # one pod
    assert pod["metadata"]["name"] == f"{instance}-0"
    assert pod["status"]["phase"] == "Running"
    assert {s["name"]: s["ready"] for s in pod["status"]["containerStatuses"]} == {
        "leaderelection": True,
        "postgres": True,
        "postgres-exporter": True,
    }

    cronjobs = json.loads(kubectl(*"get -o json cronjobs".split()).stdout)
    assert len(cronjobs["items"]) == 1

    pvc = json.loads(kubectl(*"get -o json pvc".split()).stdout)
    assert {p["metadata"]["name"] for p in pvc["items"]} == {
        f"pghome-{instance}-0",
        f"pgbackrest-home-{instance}-0",
    }

    with port_forward(namespace, instance) as (pg_port, pge_port):
        with psycopg.connect(host="localhost", user="postgres", port=pg_port) as conn:
            server_version = conn.info.server_version
            (work_mem,) = conn.execute("select setting from pg_settings where name = 'work_mem'").fetchone()  # type: ignore[misc]
        metrics = requests.get(f"http://localhost:{pge_port}/metrics").text.splitlines()

    assert server_version >= 140000
    assert int(work_mem) == 32768
    assert (
        'pg_settings_work_mem_bytes{server="localhost:5432"} 3.3554432e+07' in metrics
    )

    patchfile = tmp_path / "patch.json"
    with patchfile.open("w") as f:
        json.dump({"spec": {"configuration": {"work_mem": "4MB"}}}, f)

    with operator_running(namespace):
        kubectl(
            "patch", "instance", instance, "--type=merge", f"--patch-file={patchfile}"
        )
        time.sleep(2)
        kubectl("rollout", "status", "-w", "--timeout=60s", "statefulset", instance)

    with port_forward(namespace, instance) as (_, pge_port):
        metrics = requests.get(f"http://localhost:{pge_port}/metrics").text.splitlines()
    assert 'pg_settings_work_mem_bytes{server="localhost:5432"} 4.194304e+06' in metrics


def test_scale(kubectl: CommandRunner, namespace: str, instance: str) -> None:
    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2))
    def get_pods(expected: int) -> None:
        pods = json.loads(kubectl("get", "-o", "json", "pods").stdout)
        assert len(pods["items"]) == expected

    with operator_running(namespace):
        # Wait until the sts gets created.
        retrying(kubectl, "get", "statefulset", instance)
        kubectl("rollout", "status", "-w", "--timeout=60s", "statefulset", instance)

        kubectl("scale", "instance", instance, "--replicas=2")

        # Wait for pods to be created and then poll statefulset status.
        get_pods(2)
        kubectl("rollout", "status", "-w", "--timeout=60s", "statefulset", instance)

        logs_pg0 = kubectl("logs", f"pod/{instance}-0", "-c", "postgres").stdout
        logs_pg1 = kubectl("logs", f"pod/{instance}-1", "-c", "postgres").stdout
        assert "received replication command: SHOW data_directory_mode" in logs_pg0
        assert "started streaming WAL from primary" in logs_pg1

    cp = kubectl(
        "run",
        "--rm",
        "-it",
        "--image=pglift/postgresql",
        "--restart=Never",
        "--image-pull-policy=Never",
        "psql",
        "--",
        "psql",
        f"host=pglift-{instance}-primary user=postgres",
        "-c",
        "create temp table t as (select 1 as s); select s from t;",
    )
    assert cp.returncode == 0
    assert cp.stdout.splitlines() == [
        " s ",
        "---",
        " 1",
        "(1 row)",
        "",
        'pod "psql" deleted',
    ]

    with operator_running(namespace):
        kubectl("scale", "instance", instance, "--replicas=1")
        get_pods(1)
        kubectl("rollout", "status", "-w", "--timeout=60s", "statefulset", instance)


def test_backup(
    kubectl: CommandRunner, namespace: str, instance: str, tmp_path: pathlib.Path
) -> None:
    name = instance
    data = {
        "apiVersion": "pglift.dalibo.com/v1alpha1",
        "kind": "InstanceBackup",
        "metadata": {
            "name": name,
        },
        "spec": {
            "type": "full",
        },
    }
    fpath = tmp_path / "instance-backup.yaml"
    with fpath.open("w") as f:
        yaml.safe_dump(data, f)

    with operator_running(namespace):
        # Wait until the sts gets created.
        retrying(kubectl, "get", "statefulset", instance)
        kubectl("rollout", "status", "-w", "--timeout=60s", "statefulset", instance)

        kubectl("apply", "-f", fpath)

        time.sleep(2)
        jobs = json.loads(
            kubectl("get", "jobs.batch", "-o", "json", "-l" "backup=full").stdout
        )
        (job,) = jobs["items"]
        assert job["metadata"]["name"].startswith(f"{instance}-full-backup")

        kubectl("delete", "--wait=true", "--cascade=foreground", "-f", fpath)

        jobs = json.loads(
            kubectl("get", "jobs.batch", "-o", "json", "-l" "backup=full").stdout
        )
        assert not jobs["items"]
