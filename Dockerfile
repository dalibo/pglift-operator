FROM debian:bullseye-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -qq update && \
	apt-get -qq -y install --no-install-recommends \
	ca-certificates \
	curl \
	gnupg \
	lsb-release \
	procps \
	python3-pip \
	&& rm -rf /var/lib/apt/lists/*

ARG UID=999
ARG GID=999
RUN set -eux; \
	groupadd -r postgres --gid=${GID};\
	useradd -r -g postgres --uid=${UID} --home-dir=/var/lib/postgresql --shell=/bin/bash postgres; \
	mkdir -p /var/lib/pgbackrest; \
	chown -R postgres:postgres /var/lib/pgbackrest; \
	mkdir -p /var/lib/postgresql; \
	chown -R postgres:postgres /var/lib/postgresql

RUN echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -c -s)-pgdg main" \
	| tee /etc/apt/sources.list.d/pgdg.list
RUN curl --output /etc/apt/trusted.gpg.d/pgdg.asc https://www.postgresql.org/media/keys/ACCC4CF8.asc

RUN apt-get -qq update && \
	apt-get -y install --no-install-recommends postgresql-common
RUN mkdir -p /etc/postgresql-common/createcluster.d/
RUN echo "create_main_cluster = false" \
	> /etc/postgresql-common/createcluster.d/no-create-main-cluster.conf
RUN apt-get -qq update && \
	apt-get -qq -y install --no-install-recommends \
	libpq5 \
	pgbackrest \
	postgresql-14 \
	prometheus-postgres-exporter \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir /etc/pglift
RUN pip install --no-cache-dir click kopf kubernetes pglift psycopg[binary]  # from setup.cfg
COPY . /tmp/src
RUN pip install --no-cache-dir /tmp/src && rm -rf /tmp/src

USER postgres
WORKDIR /var/lib/postgresql
COPY src/pglift_operator/leaderelection.py /usr/local/leaderelection.py
CMD kopf run --standalone -m pglift_operator --all-namespaces
