import argparse
import json
import logging
import socket
import sys
import time
from decimal import Decimal
from typing import Any

import kubernetes.client as k
import kubernetes.config
import psycopg
import psycopg.rows
from kubernetes.leaderelection import electionconfig, leaderelection
from kubernetes.leaderelection.leaderelectionrecord import LeaderElectionRecord
from kubernetes.leaderelection.resourcelock.configmaplock import ConfigMapLock

from . import models


class PostgresHandler:
    def __init__(
        self,
        conn: psycopg.Connection[Any],
        *,
        name: str,
        namespace: str,
        identity: str,
        lsn_delay: int,
    ) -> None:
        self.conn = conn
        self.name = name
        self.namespace = namespace
        self.identity = identity
        self.api = k.CoreV1Api()
        self.lsn_delay = lsn_delay

    def set_lsn(self) -> bool:
        """Return True if storage of our node's LSN succeeded, meaning that
        the node is alive.
        """
        try:
            lsn = wal_lsn(self.conn)
        except psycopg.OperationalError as e:
            logging.error("connection to postgres failed: %s", str(e))
            return False
        else:
            set_lsn(
                self.api,
                name=self.name,
                namespace=self.namespace,
                identity=self.identity,
                lsn=lsn,
            )
            return True

    def should_take_lock(
        self, oldrecord: LeaderElectionRecord, record: LeaderElectionRecord
    ) -> bool:
        """Return True if this node should try to take the lock."""
        holder_identity = oldrecord.holder_identity

        # If we were the primary already, keep the lock.
        if self.identity == holder_identity:
            return True

        # If we are a standby, check that we have the best LSN.
        return is_best_standby(
            self.api,
            self.conn,
            name=self.name,
            namespace=self.namespace,
            identity=self.identity,
            holder_identity=holder_identity,
            delay=self.lsn_delay,
        )

    def onstarted_leading(self) -> None:
        self.conn.execute("SELECT CASE WHEN pg_is_in_recovery() THEN pg_promote() END")
        service_name = models.service_primary_name(self.name)
        svc = self.api.read_namespaced_service(service_name, namespace=namespace)
        svc.spec.selector["statefulset.kubernetes.io/pod-name"] = self.identity
        self.api.replace_namespaced_service(
            name=service_name, namespace=namespace, body=svc
        )
        logging.info("update service %s/%s", namespace, service_name)

    def onstopped_leading(self) -> None:
        cm_name = models.lsn_configmap_name(self.name)
        cm = models.lsn_configmap(self.name)
        if cm.data is not None:
            self.api.patch_namespaced_config_map(
                cm_name, namespace=namespace, body={"data": {self.identity: None}}
            )
            logging.info(
                "cleared %s from %s/%s configmap", self.identity, namespace, cm_name
            )
        sys.exit(1)


class LeaderElection(leaderelection.LeaderElection):  # type: ignore[misc]
    def __init__(
        self, election_config: electionconfig.Config, postgres_handler: PostgresHandler
    ) -> None:
        self._postgres_handler = postgres_handler
        super().__init__(election_config)

    def try_acquire_or_renew(self) -> bool:
        if not self._postgres_handler.set_lsn():
            return False
        return super().try_acquire_or_renew()  # type: ignore[no-any-return]

    def update_lock(self, record: LeaderElectionRecord) -> bool:
        _, old_record = self.election_config.lock.get(
            self.election_config.lock.name, self.election_config.lock.namespace
        )
        if old_record is None or self._postgres_handler.should_take_lock(
            old_record, record
        ):
            return super().update_lock(record)  # type: ignore[no-any-return]
        return False


def wal_lsn(conn: psycopg.Connection[Any]) -> str:
    logging.debug("getting WAL LSN")
    (lsn,) = conn.execute(
        "SELECT CASE WHEN pg_is_in_recovery()"
        " THEN pg_last_wal_replay_lsn()"
        " ELSE pg_current_wal_lsn()"
        "END"
    ).fetchone()  # type: ignore[misc]
    assert isinstance(lsn, str)
    return lsn


def set_lsn(
    api: k.CoreV1Api, *, name: str, namespace: str, identity: str, lsn: str
) -> k.V1ConfigMap:
    cm_name = models.lsn_configmap_name(name)
    now = time.monotonic_ns()
    logging.info(
        "patching configmap %s/%s: %s -> [%s, %d]",
        namespace,
        cm_name,
        identity,
        lsn,
        now,
    )
    return api.patch_namespaced_config_map(
        cm_name,
        namespace=namespace,
        body={"data": {identity: json.dumps([lsn, now])}},
    )


def is_best_standby(
    api: k.CoreV1Api,
    conn: psycopg.Connection[Any],
    *,
    name: str,
    namespace: str,
    identity: str,
    holder_identity: str,
    delay: int,
) -> bool:
    assert identity != holder_identity
    cm = api.read_namespaced_config_map(
        models.lsn_configmap_name(name), namespace=namespace
    )
    assert cm.data is not None
    now = time.monotonic_ns()
    primary_lsn, primary_time = json.loads(cm.data[holder_identity])
    primary_age = now - primary_time
    if primary_age > delay:
        logging.warning("primary LSN is old by %dns", primary_age)
    lsn, _ = json.loads(cm.data[identity])
    if lsn == primary_lsn:
        logging.info("replication lag: 0")
        return True
    lag = replication_lag(conn, primary_lsn, lsn)
    logging.info("replication lag: %f", lag)
    for stdby_i, stdby_r_info in cm.data.items():
        if stdby_i in (holder_identity, identity):
            continue
        stdby_info = json.loads(stdby_r_info)
        if stdby_info is None:
            continue
        stdby_lsn, stdby_time = stdby_info
        if now - stdby_time > delay:
            logging.info("standby %s is old", stdby_i)
            continue
        if replication_lag(conn, primary_lsn, stdby_lsn) < lag:
            logging.info("%s has a better replication lag", stdby_i)
            return False
    return True


def replication_lag(
    conn: psycopg.Connection[Any], primary_lsn: str, standby_lsn: str
) -> Decimal:
    logging.debug("computing replication lag")
    (lag,) = conn.execute(  # type: ignore[misc]
        "SELECT pg_wal_lsn_diff(%s, %s)", (primary_lsn, standby_lsn)
    ).fetchone()
    assert isinstance(lag, Decimal)
    return lag


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("name", help="instance name")
    p.add_argument("-i", "--identity", default=socket.gethostname())
    p.add_argument("-n", "--namespace")
    p.add_argument("--lease-duration", type=int, default=10)
    p.add_argument("--lease-deadline", type=int, default=8)
    p.add_argument("--retry-period", type=int, default=5)
    p.add_argument(
        "--lsn-delay",
        type=int,
        default=10,
        help="expiration delay for standby LSN in seconds (default: %(default)d)",
    )
    p.add_argument("--dsn", default="host=localhost user=postgres")

    args = p.parse_args()

    try:
        kubernetes.config.load_incluster_config()
    except kubernetes.config.config_exception.ConfigException:
        kubernetes.config.load_kube_config()

    namespace = (
        args.namespace
        or kubernetes.config.list_kube_config_contexts()[1]["context"]["namespace"]
    )

    with psycopg.connect(args.dsn, autocommit=True) as conn:
        postgres_handler = PostgresHandler(
            conn,
            name=args.name,
            namespace=args.namespace,
            identity=args.identity,
            lsn_delay=args.lsn_delay * 1e9,
        )
        config = electionconfig.Config(
            ConfigMapLock(
                models.leaderelection_configmap_name(args.name),
                namespace,
                args.identity,
            ),
            lease_duration=args.lease_duration,
            renew_deadline=args.lease_deadline,
            retry_period=args.retry_period,
            onstarted_leading=postgres_handler.onstarted_leading,
            onstopped_leading=postgres_handler.onstopped_leading,
        )
        LeaderElection(config, postgres_handler).run()
