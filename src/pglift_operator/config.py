import os
from pathlib import Path

import kubernetes.client.models as k
from pglift.settings import Settings

REGISTRY = os.environ.get("REGISTRY", "")
if REGISTRY and not REGISTRY.endswith("/"):
    REGISTRY += "/"
POSTGRES_IMAGE = f"{REGISTRY}pglift/postgresql"
PGLIFT_IMAGE = f"{REGISTRY}pglift/operator"
POSTGRES_EXPORTER_IMAGE = f"{REGISTRY}pglift/postgres_exporter"
PGVERSION = "14"
PGHOME = "/var/lib/postgresql"
PGHOME_VOLUME = "pghome"
PGHOME_SIZE = "1Gi"
PGGID = 999
BACKUP_VOLUME = "pgbackrest-home"
BACKUP_DIR = "/var/lib/pgbackrest"
BACKUP_SIZE = "1Gi"
PROMETHEUS_VOLUME = "prometheus-home"
PROMETHEUS_HOME = "/var/lib/prometheus"
PROMETHEUS_POSTGRES_EXPORTER = "/usr/bin/prometheus-postgres-exporter"
# Turn off the imagePullPolicy:Always to make docker-env work with minikube
# https://minikube.sigs.k8s.io/docs/handbook/pushing/#1-pushing-directly-to-the-in-cluster-docker-daemon-docker-env
IMAGE_PULL_POLICY = os.environ.get("IMAGE_PULL_POLICY", "Never")
IMAGE_PULL_SECRET = os.environ.get("IMAGE_PULL_SECRET")


BACKUP_BASEDIR = Path(BACKUP_DIR) / "{instance.version}-{instance.name}"
SETTINGS = Settings.parse_obj(
    {
        "logpath": "/tmp/pglift-logs",
        "prefix": PGHOME,
        "postgresql": {
            "root": PGHOME,
            "auth": {
                "passfile": str(Path(PGHOME) / ".pgpass"),
            },
        },
        "pgbackrest": {
            "configpath": str(
                Path(BACKUP_DIR) / "{instance.version}-{instance.name}.conf"
            ),
            "directory": str(BACKUP_BASEDIR),
            "logpath": str(BACKUP_BASEDIR / "logs"),
            "spoolpath": str(BACKUP_BASEDIR / "spool"),
            "lockpath": str(BACKUP_BASEDIR / "lock"),
        },
        # The following settings are needed for tests.
        "sysuser": ["postgres", "postgres"],
        "systemd": {
            "unit_path": str(Path(PGHOME) / ".local" / "share" / "systemd" / "user")
        },
    }
)


POSTGRES_EXPORTER_SETTINGS = Settings.parse_obj(
    {
        "prefix": PROMETHEUS_HOME,
        "prometheus": {
            "execpath": PROMETHEUS_POSTGRES_EXPORTER,
        },
        # The following settings are needed for tests.
        "postgresql": {
            "auth": {
                "passfile": str(Path(PROMETHEUS_HOME) / ".pgpass"),
            },
        },
        "sysuser": ["prometheus", "prometheus"],
        "systemd": {
            "unit_path": str(
                Path(PROMETHEUS_HOME) / ".local" / "share" / "systemd" / "user"
            )
        },
    }
)


def image_pull_secrets() -> list[k.V1LocalObjectReference]:
    if IMAGE_PULL_SECRET is None:
        return []
    return [k.V1LocalObjectReference(name=IMAGE_PULL_SECRET)]
